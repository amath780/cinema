import { createRouter, createWebHistory } from 'vue-router'
import AccueilFilm from "@/views/AccueilFilm.vue";
import RechercheFilm from "@/views/RechercheFilm.vue";
import MovieDetails from '@/components/MovieDetails.vue';
import ActorDetails from "@/components/ActorDetails.vue"; // Importez votre composant de détails de film


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
   /* {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },*/
    {
      path: '/',
      name: 'home',
      component: AccueilFilm
    },
    {
      path: '/recherche',
      name: 'recherche',
      component: RechercheFilm
    },
    {
      path: '/movie/:id', // Chemin de l'URL avec un paramètre dynamique pour l'ID du film
      name: 'MovieDetails', // Nom de la route
      component: MovieDetails, // Composant à afficher lorsque cette route est visitée
      props: true // Transférer les paramètres en tant que propriétés au composant
    },
    {
      path: '/actor/:id', // Chemin de l'URL avec un paramètre dynamique pour l'ID du film
      name: 'ActorDetails', // Nom de la route
      component: ActorDetails, // Composant à afficher lorsque cette route est visitée
      props: true // Transférer les paramètres en tant que propriétés au composant
    }
  ]
})

export default router
